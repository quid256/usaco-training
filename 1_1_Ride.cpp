/*
ID: quidnov1
PROG: ride
LANG: C++11
*/
#include <iostream>
#include <fstream>
#include <string>
// #include <ctime>

using namespace std;

int main() {
    ofstream fout ("ride.out");
    ifstream fin ("ride.in");
    // clock_t t;
    // t = clock();
    
    string comet, group;
    fin >> comet >> group;

    int cometProd = 1;
    int groupProd = 1;
    
    for (int i = 0; i < comet.length(); i++) {
    	cometProd *= comet[i] - 'A' + 1;
    }

    for (int i = 0; i < group.length(); i++) {
    	groupProd *= group[i] - 'A' + 1;
    }

    // cout << cometProd << endl;
    // cout << groupProd << endl;

    if ((cometProd % 47) == (groupProd % 47)) {
    	fout << "GO";
    } else {
    	fout << "STAY";
    }

    fout << endl;

    // t = clock() - t;
    // cout << "Time: " << ((float)t) / CLOCKS_PER_SEC << endl;
    return 0;
}
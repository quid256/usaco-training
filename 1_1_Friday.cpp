/*
ID: quidnov1
PROG: friday
LANG: C++
*/

#include <fstream>

using namespace std;

int main() {
	ofstream fout ("friday.out");
	ifstream fin ("friday.in");

	int numYears;
	fin >> numYears;

	int curDay = 2;
	int dayCounts[] = {0, 0, 0, 0, 0, 0, 0};
	const int monthLengths[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	for (int yearNum = 1900; yearNum < 1900 + numYears; yearNum++) {
		bool isLeap = false;
		if (yearNum % 4 == 0)
			isLeap = true;
		if (yearNum % 100 == 0)
			isLeap = false;
		if (yearNum % 400 == 0)
			isLeap = true;

		for (int monthNum = 0; monthNum < 12; monthNum++) {

			dayCounts[(curDay + 12) % 7] += 1;

			curDay = (curDay + monthLengths[monthNum]) % 7;
			if (isLeap && monthNum == 1)
				curDay = (curDay + 1) % 7;
		}
		
	}

	fout << dayCounts[0];
	for (int j = 1; j < 7; j++) {
		fout << " " << dayCounts[j];
	}

	fout << endl;

	return 0;
}
/*
ID: quidnov1
PROG: beads
LANG: C++
*/
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>

using namespace std;

int mod(int a, int b)
{ return (a%b+b)%b; }

int main() {
	ofstream fout ("beads.out");
	ifstream fin ("beads.in");
	
	int numBeads;
	fin >> numBeads;

	string beadPattern;
	fin >> beadPattern;

	int maxLength = -1;
	int maxPos = -1;

	int i = 0;
	for (i = 0; i < numBeads; i++) {
		int rightLength = 0;
		while (beadPattern[(i + rightLength) % numBeads] == 'w') {
			rightLength = (rightLength + 1);
			if (rightLength >= numBeads)
			{
				break;
			}
		}
		if (rightLength < numBeads) {
			char rightBeadColor = beadPattern[(i + rightLength) % numBeads];
			while (beadPattern[(i + rightLength) % numBeads] == 'w' || beadPattern[(i + rightLength) % numBeads] == rightBeadColor)
			{
				rightLength = (rightLength + 1);
				if (rightLength >= numBeads)
					break;
			}
		}
		
		int endRightPos = (i + rightLength) % numBeads;

		int leftLength = 0;

		if (rightLength < numBeads) {
			
			while (beadPattern[mod((i - leftLength - 1), numBeads)] == 'w') {
				leftLength = (leftLength + 1);
				if (leftLength >= numBeads - rightLength)
					break;
			}
				
			char leftBeadColor = beadPattern[mod((i - leftLength - 1), numBeads)];
			while (beadPattern[mod((i - leftLength - 1), numBeads)] == 'w' || beadPattern[mod((i - leftLength - 1), numBeads)] == leftBeadColor) {
				leftLength = (leftLength + 1);
				if (leftLength >= numBeads - rightLength)
					break;
			}
		}
		if (leftLength + rightLength > maxLength) {
			maxLength = leftLength + rightLength;
			maxPos = i;
		}
	}
	fout << maxLength << endl;

	return 0;
}
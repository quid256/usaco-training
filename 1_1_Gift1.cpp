/*
ID: quidnov1
PROG: gift1
LANG: C++
*/
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>


using namespace std;

int main() {
	ofstream fout ("gift1.out");
	ifstream fin ("gift1.in");
	
	int NP;
	fin >> NP;

	map<string, int> nameList;
	vector<string> names (NP);
	
	bool linus = false;

	for (int i = 0; i < NP; i++) {
		string name;
		fin >> name;
		nameList[name] = 0;
		names[i] = name;

		if (name == "Linus") {
			linus = true;
		}
	}

	string giftGiver;
	int giftAmount, numRecipients;

	for (int j = 0; j < NP; j++) {
		fin >> giftGiver;
		fin >> giftAmount >> numRecipients;
		if (giftAmount != 0 && numRecipients == 0) {
			nameList[giftGiver] += giftAmount;
			continue;
		}
		if (numRecipients != 0) {
			nameList[giftGiver] += (giftAmount % numRecipients) - giftAmount;
			if (giftGiver == "Linus") {
				cout << "Gave: " << (giftAmount % numRecipients) - giftAmount << endl;
				cout << "Now Has: " << nameList["Linus"] << endl;
			}
			for (int i = 0; i < numRecipients; i++) {
				string recipientName;
				fin >> recipientName;
				nameList[recipientName] += giftAmount / numRecipients;

				if (recipientName == "Linus") {
					cout << "Gave: " << giftAmount / numRecipients << endl;
					cout << "Now Has: " << nameList["Linus"] << endl;
				}
			}
		}

	}

	if (linus)
		cout << nameList["Linus"];

	for (vector<string>::iterator i = names.begin(); i != names.end(); i++) {
		fout << *i << " " << nameList[*i] << "\n";
	}

	return 0;
}